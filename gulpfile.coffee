gulp = require 'gulp'
coffee = require 'gulp-coffee' 
uglify = require 'gulp-uglify' #min js
clean = require 'gulp-clean' #clean extra files
rjs = require 'gulp-requirejs' #modules
browserSync = require 'browser-sync'
nodemon = require 'gulp-nodemon'

gulp.task 'browserSync', ['start'], ->
	browserSync.init null, {
		proxy: 'http://localhost:5000',
		files: ['public/', 'serverjs/'],
		browser: 'firefox'
		port: 7000
	}

gulp.task 'start', ['buildAll'], (cb) ->
	started = no
	stream = (nodemon {
		script: 'serverjs/index.js'
	}).on 'start', ->
	unless started
		do cb
		started = on
	
gulp.task 'coffee-server', ->
	gulp.src 'coffee/server/*.coffee'
		.pipe do coffee
		.pipe gulp.dest 'serverjs'

gulp.task 'coffee-client', ->
	gulp.src 'coffee/client/model/*.coffee'
		.pipe do coffee
		.pipe gulp.dest 'js'

gulp.task 'build-client', ['coffee-client'], ->
	rjs
		baseUrl: 'js'
		name: '../bower_components/almond/almond' #almond allow to attach all modules with one file
		include: ['main']
		insertRequire: ['main'] #access point to the app
		out: 'all.js' #final file
		wrap: on #function wrap over the code
	.pipe do uglify
	.pipe gulp.dest 'public/js'
	gulp.src 'js/', read: no
		.pipe do clean

gulp.task 'watch', ->
	gulp.watch 'coffee/server/*.coffee', ['coffee-server']
	gulp.watch 'coffee/client/model/*.coffee', ['build-client']

gulp.task 'buildAll', ['coffee-server', 'build-client', 'watch']
gulp.task 'default', ['browserSync']