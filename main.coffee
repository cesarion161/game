class Vec2
	constructor: (x, y) ->
		@x = x ? 0
		@y = y ? 0
	add: (vec) ->
		@x += vec.x
		@y += vec.y
		return @
	copy: -> new Vec2 @x, @y
	@getRandom: (min, max) ->
		new Vec2 do Math.random * (max - min) + min,
			do Math.random * (max - min) + min

class World
	constructor: (@canvas) ->
		@ctx = @canvas.getContext '2d'
		@canvasWidth = @canvas.width = window.innerWidth
		@canvasHeight = @canvas.height = window.innerHeight

		@objects = []
		@controllable = []
		@mouse = new Vec2 @canvasWidth / 2, @canvasHeight / 2

		@params = 
			gravity: new Vec2 0, -.2

		@canvas.addEventListener "mousemove", ((e) =>
			[@mouse.x, @mouse.y] = [e.offsetX, e.offsetY]
		), no

		@canvas.addEventListener "mousewheel", ((e) =>
			do e.preventDefault
			for element in @controllable
				if element instanceof ParticleSystem
					if e.shiftKey
						element.scatter = Math.max 0, element.scatter - e.wheelDelta / 300
					else if e.altKey
						element.particleSize = Math.max 0, element.particleSize - e.wheelDelta / 100
					else
						element.particleLife = Math.max 1, element.particleLife - e.wheelDelta / 10
		), no

		window.addEventListener "resize", ((e) =>
			@canvasWidth = @canvas.width = window.innerWidth
			@canvasHeight = @canvas.height = window.innerHeight
			[@mouse.x, @mouse.y] = [@canvasWidth / 2, @canvasHeight / 2]
		), no
	getRandomColor: ->
		[
			[0, "rgba(#{Math.floor (do Math.random * 255)},#{Math.floor (do Math.random * 255)},#{Math.floor (do Math.random * 255)},.#{Math.floor (do Math.random * 10)})"],
			[do Math.random, "rgba(#{Math.floor (do Math.random * 255)},#{Math.floor (do Math.random * 255)},#{Math.floor (do Math.random * 255)},.#{Math.floor (do Math.random * 10)})"],
			[1, "transparent"]
		]
	addObject: (constructor, config, controllable) ->
		config.world = @
		obj = new constructor config
		do obj.setControllable if controllable
		@objects.push obj

	removeObject: (index) -> @objects.splice index, 1

	start: -> do @tick

	tick: ->
		do @update
		do @draw
		requestAnimationFrame @tick.bind @

	update: ->
		object.update ind for object, ind in @objects when object

	draw: ->
		@ctx.clearRect 0, 0, @canvasWidth, @canvasHeight
		@ctx.globalAlpha = 1
		do object.draw for object in @objects

class _Object
	constructor: (config) ->
		@loc = config.loc ? new Vec2
		@speed = config.speed ? new Vec2
		@world = config.world

	update: ->
		unless @ instanceof ParticleSystem
			@speed.add @world.params.gravity
		@loc.add @speed

	notVisible: (threshold) ->
		@loc.y > @world.canvasHeight + threshold or
		@loc.y < -threshold or
		@loc.x > @world.canvasWidth + threshold or
		@loc.x < -threshold

	setControllable: ->
		@world.controllable.push @
		@loc = @world.mouse

class ParticleSystem extends _Object
	constructor: (config) ->
		super config
		@particles = []
		@maxParticles = config.maxParticles ? 300
		@particleLife = config.particleLife ? 60
		@particleSize = config.particleSize ? 24
		@creationRate = config.creationRate ? 3
		@scatter = config.scatter ? 1.3 #разброс
		@colors = config.colors ? do @world.getRandomColor

	addParticle: (config) ->
		config.system = @
		config.world = @world
		@particles.push new Particle config

	removeParticle: (index) -> @particles.splice index, 1

	update: ->
		unless @particles.length > @maxParticles
			for i in [0..@creationRate]
				@addParticle {
					loc: do @loc.copy
					speed: Vec2.getRandom -@scatter, @scatter
				}
		particle.update ind for particle, ind in @particles when particle

	draw: ->
		particle.draw @colors for particle in @particles

class Particle extends _Object
	constructor: (config) ->
		super config
		@system = config.system
		@initialLife = @system.particleLife
		@life = @initialLife
		@size = @system.ParticleSize

	update: (ind) ->
		super
		@size = Math.max 0, @system.particleSize * (@life-- / @initialLife)
		if @notVisible 100 or @life < 0 then @system.removeParticle ind

	draw: (colors) ->
		@world.ctx.globalCompositeOperation = "lighter"
		@world.ctx.globalAlpha = @life / @initialLife

		grad = @world.ctx.createRadialGradient @loc.x, @loc.y, 0, @loc.x, @loc.y, @size
		for color in colors
			grad.addColorStop color[0], color[1]
		@world.ctx.fillStyle = grad

		do @world.ctx.beginPath
		@world.ctx.arc @loc.x, @loc.y, @size, 0, 2 * Math.PI
		do @world.ctx.fill

test = new World document.getElementById "canvas"
window.test = test

test.addObject ParticleSystem, {
	loc: new Vec2 50, 50
	particleSize: 25
	particleLife: 55
	scatter: .7
}, on

test.addObject ParticleSystem, {
	loc: new Vec2 50, 50
	particleSize: 15
	particleLife: 85
	scatter: 1.4
}, on

do test.start
