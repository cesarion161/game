(function() {
  var ParticleSystem, Player, Vec2, World, app, express, getTime, http, io, server, statica, wld;

  http = require('http');

  express = require('express');

  app = express();

  World = require('./World.js');

  ParticleSystem = require('./ParticleSystem.js');

  Player = require('./Player.js');

  Vec2 = require('./Vec2.js');

  wld = new World();

  getTime = function() {
    return (new Date).toLocaleTimeString();
  };

  app.disable('x-powered-by');

  app.use(function(req, res, next) {
    console.log(getTime(), req.method, req.connection.remoteAddress, req.url);
    return next();
  });

  statica = __dirname + '/../public/';

  app.use(express["static"](statica));

  server = app.listen(5000, function() {
    return console.log('Listening on port 5000');
  });

  app.get('/', function(req, res) {
    return res.sendFile(statica + 'index.html');
  });

  io = (require('socket.io'))(server);

  io.sockets.on('connection', function(socket) {
    var Id, time;
    Id = socket.io.toString().substr(0, 7);
    time = getTime();
    wld.addPlayer(new Player(Vec2.getRandom(0, wld.width)), wld);
    socket.broadcast.json.send({
      event: 'newPlayer',
      players: wld.players,
      time: time
    });
    socket.on('move', function(newloc) {});
    return socket.on('disconnect', function() {
      wld.removeBySocket(Id);
      return socket.broadcast.json.send({
        event: 'disconnected',
        players: wld.players,
        time: time
      });
    });
  });

}).call(this);
