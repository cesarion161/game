(function() {
  var ParticleSystem;

  module.exports = ParticleSystem = (function() {
    function ParticleSystem(player, config) {
      var ref, ref1, ref2, ref3, ref4, ref5;
      this.maxParticles = (ref = config.maxParticles) != null ? ref : 300;
      this.particleLife = (ref1 = config.particleLife) != null ? ref1 : 60;
      this.particleSize = (ref2 = config.particleSize) != null ? ref2 : 24;
      this.creationRate = (ref3 = config.creationRate) != null ? ref3 : 3;
      this.scatter = (ref4 = config.scatter) != null ? ref4 : 1.3;
      this.player = player;
      this.colors = (ref5 = config.colors) != null ? ref5 : this.player.world.getRandomColor();
    }

    return ParticleSystem;

  })();

}).call(this);
