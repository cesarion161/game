(function() {
  var Player, Vec2, World;

  Player = require('./Player.js');

  Vec2 = require('./Vec2.js');

  module.exports = World = (function() {
    function World(players, particleSystems, config) {
      this.players = players != null ? players : [];
      this.particleSystems = particleSystems != null ? particleSystems : [];
      this.params = config != null ? config : {
        maxplayers: 10,
        width: 2000,
        height: 2000,
        gravity: new Vec2(0, -.2)
      };
    }

    World.prototype.getRandomColor = function() {
      return [[0, "rgba(" + (Math.floor(Math.random() * 255)) + "," + (Math.floor(Math.random() * 255)) + "," + (Math.floor(Math.random() * 255)) + ",." + (Math.floor(Math.random() * 10)) + ")"], [Math.random(), "rgba(" + (Math.floor(Math.random() * 255)) + "," + (Math.floor(Math.random() * 255)) + "," + (Math.floor(Math.random() * 255)) + ",." + (Math.floor(Math.random() * 10)) + ")"], [1, "transparent"]];
    };

    World.prototype.addObject = function(constructor, config) {
      var obj;
      config.world = this;
      obj = new constructor(config);
      return this.objects.push(obj);
    };

    World.prototype.addPlayer = function(player) {
      return this.players.push(player);
    };

    World.prototype.removePlayer = function(index) {
      return this.players.splice(index, 1);
    };

    World.prototype.removeBySocket = function(socket) {
      var i, j, len, player, results;
      results = [];
      for (i = j = 0, len = players.length; j < len; i = ++j) {
        player = players[i];
        if (player.socket === socket) {
          results.push(removePlayer(i));
        }
      }
      return results;
    };

    World.prototype.update = function() {
      var j, len, player, ref, results;
      ref = this.players;
      results = [];
      for (j = 0, len = ref.length; j < len; j++) {
        player = ref[j];
        results.push(player.update());
      }
      return results;
    };

    return World;

  })();

}).call(this);
