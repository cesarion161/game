(function() {
  var ParticleSystem, Player;

  ParticleSystem = require('./ParticleSystem.js');

  module.exports = Player = (function() {
    function Player(socket, loc, world, systems) {
      this.socket = socket;
      this.loc = loc;
      this.world = world;
      this.particleSystems = systems != null ? systems : [].push(new ParticleSystem(this));
    }

    Player.prototype.moveTo = function(newloc) {
      return this.loc = newloc;
    };

    return Player;

  })();

}).call(this);
