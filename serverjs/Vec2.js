(function() {
  var Vec2;

  module.exports = Vec2 = (function() {
    function Vec2(x, y) {
      this.x = x != null ? x : 0;
      this.y = y != null ? y : 0;
    }

    Vec2.prototype.add = function(vec) {
      this.x += vec.x;
      this.y += vec.y;
      return this;
    };

    Vec2.prototype.copy = function() {
      return new Vec2(this.x, this.y);
    };

    Vec2.getRandom = function(min, max) {
      return new Vec2(Math.random() * (max - min) + min, Math.random() * (max - min) + min);
    };

    return Vec2;

  })();

}).call(this);
