ParticleSystem = require './ParticleSystem.js'
module.exports = 
class Player
	constructor: (socket, loc, world, systems) ->
		@socket = socket
		@loc = loc
		@world = world
		@particleSystems = systems ? [].push (new ParticleSystem @)
	moveTo: (newloc) ->
		@loc = newloc