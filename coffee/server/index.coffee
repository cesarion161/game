http = require 'http'
express = require 'express'
app = do express

World = require './World.js'
ParticleSystem = require './ParticleSystem.js'
Player = require './Player.js'
Vec2 = require './Vec2.js'

wld = new World()
getTime = ->
	(new Date).toLocaleTimeString()
app.disable 'x-powered-by'
app.use (req, res, next) -> 
	console.log do getTime, req.method, req.connection.remoteAddress, req.url
	do next
statica = __dirname + '/../public/'
app.use express.static statica
server = app.listen 5000, ->
	console.log 'Listening on port 5000'

app.get '/', (req, res) ->
	res.sendFile statica + 'index.html'

io = (require 'socket.io') server
io.sockets.on 'connection', (socket) ->
	Id = (socket.io).toString().substr(0, 7)
	time = do getTime
	wld.addPlayer(new Player(Vec2.getRandom 0, wld.width), wld)
	socket.broadcast.json.send {event: 'newPlayer', players: wld.players, time: time}

	socket.on 'move', (newloc) ->
		

	socket.on 'disconnect', ->
		wld.removeBySocket Id
		socket.broadcast.json.send {event: 'disconnected', players: wld.players, time: time}
