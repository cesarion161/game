Player = require './Player.js'
Vec2 = require './Vec2.js'
module.exports =
class World
	constructor: (players, particleSystems, config) ->
		@players = players ? []
		@particleSystems = particleSystems ? []
		@params = config ? {
			maxplayers : 10
			width : 2000
			height : 2000
			gravity: new Vec2 0, -.2
		}
	getRandomColor: ->
		[
			[0, "rgba(#{Math.floor (do Math.random * 255)},#{Math.floor (do Math.random * 255)},#{Math.floor (do Math.random * 255)},.#{Math.floor (do Math.random * 10)})"],
			[do Math.random, "rgba(#{Math.floor (do Math.random * 255)},#{Math.floor (do Math.random * 255)},#{Math.floor (do Math.random * 255)},.#{Math.floor (do Math.random * 10)})"],
			[1, "transparent"]
		]
	addObject: (constructor, config) ->
		config.world = @
		obj = new constructor config
		@objects.push obj
	addPlayer: (player) ->
		@players.push player

	removePlayer: (index) -> @players.splice index, 1

	removeBySocket: (socket) ->
		removePlayer i for player, i in players when player.socket == socket

	update: -> do player.update for player in @players