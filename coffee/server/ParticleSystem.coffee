module.exports =
class ParticleSystem
	constructor: (player, config) ->
		@maxParticles = config.maxParticles ? 300
		@particleLife = config.particleLife ? 60
		@particleSize = config.particleSize ? 24
		@creationRate = config.creationRate ? 3
		@scatter = config.scatter ? 1.3 #разброс
		@player = player
		@colors = config.colors ? do @player.world.getRandomColor