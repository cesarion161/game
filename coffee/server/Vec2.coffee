module.exports =
class Vec2
	constructor: (x, y) ->
		@x = x ? 0
		@y = y ? 0
	add: (vec) ->
		@x += vec.x
		@y += vec.y
		return @
	copy: -> new Vec2 @x, @y
	@getRandom: (min, max) ->
		new Vec2 do Math.random * (max - min) + min,
			do Math.random * (max - min) + min