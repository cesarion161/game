define ['Vec2'], (Vec2) ->
class World
	constructor: (@canvas) ->
		@ctx = @canvas.getContext '2d'
		@canvasWidth = @canvas.width = window.innerWidth
		@canvasHeight = @canvas.height = window.innerHeight

		@objects = []
		@controllable = []
		@mouse = new Vec2 @canvasWidth / 2, @canvasHeight / 2 #Here must get from server

		@params = 
			gravity: new Vec2 0, -.2

		@canvas.addEventListener "mousemove", ((e) =>
			[@mouse.x, @mouse.y] = [e.offsetX, e.offsetY]
		), no

		@canvas.addEventListener "mousewheel", ((e) =>
			do e.preventDefault
			for element in @controllable
				if element instanceof ParticleSystem
					if e.shiftKey
						element.scatter = Math.max 0, element.scatter - e.wheelDelta / 300
					else if e.altKey
						element.particleSize = Math.max 0, element.particleSize - e.wheelDelta / 100
					else
						element.particleLife = Math.max 1, element.particleLife - e.wheelDelta / 10
		), no

		window.addEventListener "resize", ((e) =>
			@canvasWidth = @canvas.width = window.innerWidth
			@canvasHeight = @canvas.height = window.innerHeight
			[@mouse.x, @mouse.y] = [@canvasWidth / 2, @canvasHeight / 2]
		), no
	getRandomColor: ->
		[
			[0, "rgba(#{Math.floor (do Math.random * 255)},#{Math.floor (do Math.random * 255)},#{Math.floor (do Math.random * 255)},.#{Math.floor (do Math.random * 10)})"],
			[do Math.random, "rgba(#{Math.floor (do Math.random * 255)},#{Math.floor (do Math.random * 255)},#{Math.floor (do Math.random * 255)},.#{Math.floor (do Math.random * 10)})"],
			[1, "transparent"]
		]
	addObject: (constructor, config, controllable) ->
		config.world = @
		obj = new constructor config
		do obj.setControllable if controllable
		@objects.push obj

	removeObject: (index) -> @objects.splice index, 1

	start: -> do @tick

	tick: ->
		do @update
		do @draw
		requestAnimationFrame @tick.bind @

	update: ->
		object.update ind for object, ind in @objects when object

	draw: ->
		@ctx.clearRect 0, 0, @canvasWidth, @canvasHeight
		@ctx.globalAlpha = 1
		do object.draw for object in @objects