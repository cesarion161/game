define ['_Object'], (_Object) ->
class Particle extends _Object
	constructor: (config) ->
		super config
		@system = config.system
		@initialLife = @system.particleLife
		@life = @initialLife
		@size = @system.ParticleSize

	update: (ind) ->
		super
		@size = Math.max 0, @system.particleSize * (@life-- / @initialLife)
		if @notVisible 100 or @life < 0 then @system.removeParticle ind

	draw: (colors) ->
		@world.ctx.globalCompositeOperation = "lighter"
		@world.ctx.globalAlpha = @life / @initialLife

		grad = @world.ctx.createRadialGradient @loc.x, @loc.y, 0, @loc.x, @loc.y, @size
		for color in colors
			grad.addColorStop color[0], color[1]
		@world.ctx.fillStyle = grad

		do @world.ctx.beginPath
		@world.ctx.arc @loc.x, @loc.y, @size, 0, 2 * Math.PI
		do @world.ctx.fill
