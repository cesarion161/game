define ['_Object', 'Particle'], (_Object, Particle) ->
class ParticleSystem extends _Object
	constructor: (config) ->
		super config
		@particles = []
		@maxParticles = config.maxParticles ? 300
		@particleLife = config.particleLife ? 60
		@particleSize = config.particleSize ? 24
		@creationRate = config.creationRate ? 3
		@scatter = config.scatter ? 1.3 #разброс
		@colors = config.colors ? do @world.getRandomColor

	addParticle: (config) ->
		config.system = @
		config.world = @world
		@particles.push new Particle config

	removeParticle: (index) -> @particles.splice index, 1

	update: ->
		unless @particles.length > @maxParticles
			for i in [0..@creationRate]
				@addParticle {
					loc: do @loc.copy
					speed: Vec2.getRandom -@scatter, @scatter
				}
		particle.update ind for particle, ind in @particles when particle

	draw: ->
		particle.draw @colors for particle in @particles