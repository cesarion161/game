define ['Vec2'], (Vec2) ->
class _Object
	constructor: (config) ->
		@loc = config.loc ? new Vec2
		@speed = config.speed ? new Vec2
		@world = config.world

	update: ->
		unless @ instanceof ParticleSystem
			@speed.add @world.params.gravity
		@loc.add @speed

	notVisible: (threshold) ->
		@loc.y > @world.canvasHeight + threshold or
		@loc.y < -threshold or
		@loc.x > @world.canvasWidth + threshold or
		@loc.x < -threshold

	setControllable: ->
		@world.controllable.push @
		@loc = @world.mouse
